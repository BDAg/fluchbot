<img src="public/fluchbot-banner.jpg"/>

## Descrição
O FluchBot é um ***Web Scraping*** e **disparador de mensagem** feito para Whatsapp Web. O software tem o objetivo de automatizar o envio de mensagens no chat de cada contato e a captura de leads através do scraping (termo que se refere à raspagem) feito em grupos do nicho de mercado da empresa, inserindo os dados dos contatos dentro de um arquivo de dados do tipo CSV.

## Quais ferramentas foram usadas para o software?
O FluchBot foi feito com duas linguagens: **Javascript** (responsável pela interface gráfica de software Desktop) e **Python** (responsável pelas funcionalidades). Para abrir o Web Whatsapp e executar comandos dentro do navegador Chrome, foi utilizado o Selenium em sua versão para Python.Para a raspagem dos dados, a biblioteca BeautifulSoup foi usada. A interface gráfica foi feita pelo Electron, que é um Framework que transforma HTML em um software desktop. Para estilização, CSS foi a linguagem escolhida, e para a renderização e desenvolvimento dos componentes, Javascript foi a ferramenta utilizada. Logo, para executar este Web Bot, você precisará dos seguintes requisitos:

#### Linguagens de Programação:
- [Python](https://www.python.org/) 3.8.5 ou versão maior;
- [Node.js](https://nodejs.org/en/)
#### Dependências do Python:
- [Selenium](https://selenium-python.readthedocs.io/) (Com o *pip* instalado, você conseguirá executar o seguinte código para instalar esta biblioteca: `pip install selenium`)
- [BeautifulSoup](https://www.crummy.com/software/BeautifulSoup/bs4/doc/) (Também pode ser instalado através do pip através do código: `pip install BeautifulSoup`)
#### Dependências do Javascript:
- [Glob](https://www.npmjs.com/package/glob) 
- [Electron](https://www.electronjs.org/)

## Executando:
Para começar, você deve instalar todas as linguagens de programação e dependências do Python. Após isso, é necessário o download do [Chromedriver](chromedriver.chromium.org/downloads) para a versão de seu Google Chrome (Veja como achar a versão [clicando aqui!](https://screencorp.zendesk.com/hc/pt-br/articles/115001590211-Visualizando-a-vers%C3%A3o-do-Google-Chrome)). Coloque o arquivo baixado dentro da pasta do projeto. Agora, com o Node.js instalado, você também terá em seu computador alguma versão do npm. Então, execute o seguinte comando no terminal DENTRO DA PASTA DO PROJETO:

`npm install`

Logo após, o software já poderá ser executado a partir do comando:

`npm start`

Se tudo estiver certo, você verá a seguinte imagem:
<br>
<img src="public/print-tela-inicial.png" height=300/>
<br>
Agora é só utilizar como quiser :)
## Desenvolvedores do Projeto:
- [Dalton Pacola Franco](https://gitlab.com/daltonpf1)
- Daniel Henrique dos Santos
- [Elídio Laureano Filho](https://gitlab.com/el34)
- [Daniel Batistão de Paula](https://www.linkedin.com/in/daniel-batistao/)
## Mentor
- Prof. Dr. Luís Hilário Tobler Garcia

Você pode assistir o pitch de nosso projeto clicando [aqui!](https://www.youtube.com/watch?v=nvcWrXdOdtM)
