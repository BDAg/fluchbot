/* Script responsável por renderizar o HTML
 e dar dinamismo à interface do FluchBot */

// Carregando as bibliotecas usadas

var glob = require('glob');

 // Carregar o container principal:

var mainContainer = document.querySelector("#main-container");

 // Página Principal:

var homepage = {
	obj: document.createElement("div"),
	id: "homepage",
	label:"Regras: <br> 1º - Não seja admin do grupo que você quer capturar os contatos \n <br> 2º - O grupo para captura de contatos deve ter o botão 'Ver Mais' (Mínimo de 8 pessoas). ",

	labelForButtons: function() {
		span = document.createElement("span");
		span.innerHTML = "Escolha uma das funções: ";

		return span;
	},
	
	rules: function() {
		span = document.createElement("div");
		span.innerHTML = this.label;

		span.setAttribute("id", "rules");

		return span;
	},

	build: function() {
		// Responsável por construir a tela
		this.obj.setAttribute("id", this.id);
		mainContainer.appendChild(this.obj);
		},
	
	minimize: function() {
		// Sai da página inicial para renderizar outra
		mainContainer.removeChild(this.obj);
	},

	createButtons: function() {
		
		//Leva o usuário à página de captura de mensagens
		buttonGetMessages = document.createElement("button");
		buttonGetMessages.onclick = function() {
			unit.build(0);
			homepage.minimize()
		}

		buttonGetMessages.innerHTML = "Capturar Mensagens de Um Grupo";

		//Leva o usuário à página de captura de contatos de grupos
		buttonGetContacts = document.createElement("button");
		buttonGetContacts.onclick = function() {
			unit.build(1); // 0 - getMessage e 1 - getContacts
			homepage.minimize()
		}

		buttonGetContacts.innerHTML = "Capturar Informações de Contato de Um Grupo";

		buttonSendMessage = document.createElement("button");
		buttonSendMessage.onclick = function() {
			unit.build(2); // 0 - getMessage e 1 - getContacts
			homepage.minimize()
		}

		buttonSendMessage.innerHTML = "Mandar mensagem para os contatos de algum CSV";
		this.obj.appendChild(this.labelForButtons());
		
		this.obj.appendChild(buttonGetMessages);
		this.obj.appendChild(buttonGetContacts);
		this.obj.appendChild(buttonSendMessage);

		this.obj.appendChild(this.rules());
		

	}
}

// Botão de voltar
var backButton = {
	tag: document.createElement("i"),
	render: function() {
		mainContainer.appendChild(this.tag);
		this.tag.onclick = function() {
			mainContainer.innerHTML = "<img src='./public/logo-fluchbot.png' id='logo'/>";
			homepage.build();
		}
		this.tag.setAttribute("class", "fa fa-arrow-left")
		this.tag.setAttribute("id", "back-button")
	},
	remove: function() {
		mainContainer.removeChild(this.tag);
	}

}
 // Capturar mensagens:

var unit = {
	obj: document.createElement("div"),
	type: 0,
	label1: function() {
		span1 = document.createElement("span");
		span1.innerHTML = "Nome do grupo (Exatamente como no Whatsapp): ";

		return span1;
	},

	build: function(what) {
		this.obj = document.createElement("div");
		
		this.obj.setAttribute("id", "getpage");	

		if(what == 0){
			this.type = 0;
		}
		else if(what == 1) {
			this.type = 1;
		}
		else if(what == 2){
			this.type = 2;
		}
		
		mainContainer.appendChild(this.obj);

		this.createTheElements();
		backButton.render()
	},

	executeScript: function(nomeGrupo) {
		// Executa o script python que coleta os dados
		var python;
		alert("O bot está sendo executado. Para evitar erros, não utilize seu computador até o navegador fechar!")
		
		// 0 = Armazenar as mensagens e 1 = Armazenar os contatos, 2 = Mandar mensagem
		if (this.type == 0){
			var python = require('child_process').spawn('python3', ['./python/armazenar_msg_de_um_grupo.py', nomeGrupo]);
		}
		else if(this.type == 1) {
			var python = require('child_process').spawn('python3', ['./python/gravar_grupos.py', nomeGrupo])
		}
		else if(this.type == 2) {
		    var fileToSend = "semarquivo"
			var messageToSend = document.querySelector("#message").value;
			var hasCsvFiles = document.querySelector("#arquivoscsv").value != "";
			var csvFile = document.querySelector("#arquivoscsv").value;
			try {
                fileToSend = document.querySelector("#fileInput").files[0].path;
			} catch(e) {
                console.log("O usuário não passou nenhum arquivo.")
			}



			if(messageToSend != "" && hasCsvFiles) {
				var python = require('child_process').spawn('python3', ['./python/mandar_msg.py', messageToSend, csvFile, fileToSend]);
			}

			else {
				alert("Mensagem vazia ou não existe arquivos csv para selecionar! Volte e capture contatos de algum grupo!");
			}
			
		}
		
		python.stdout.on('data', function (data) {
			console.log("Python response: ", data.toString('utf8'));
		});

		python.stderr.on('data', (data) => {
			console.error(`stderr: ${data}`);
		});

		python.on('close', (code) => {
			console.log(`child process exited with code ${code}`);

			if(unit.type != 2) {
				var http = new XMLHttpRequest();
				try {
					http.open('HEAD', `msgs${nomeGrupo}.csv`, false)
					http.send()
					var fileExist = http.status != 404;
					if(fileExist) {
						alert("Arquivo CSV com os dados criado com sucesso!");
						unit.exit();
						homepage.build();
					}
					else {
						alert("Criação de arquivo CSV sem sucesso!");
					}
				} catch(e) {
					
					console.log(e);
					alert("O arquivo não foi criado!");
				}
			}
			else {
			    alert("Mensagens enviadas com sucesso!")
			    unit.exit();
			    homepage.build();
			}

			
			
			
		});


	},

	createTheElements: function() {
		buttonSubmit = document.createElement("button");
		if (this.type == 0) {
			buttonSubmit.innerHTML = "Capturar Mensagens";
		}
		else if(this.type == 1) {
			buttonSubmit.innerHTML = "Capturar Contatos";	
		}

		else if(this.type == 2) {
			buttonSubmit.innerHTML = "Mandar Mensagem";	
		}
		
		inputMsg = document.createElement("textarea");
		inputMsg.setAttribute("id", "message");
		inputMsg.placeholder = "Escreva a mensagem aqui!";

		inputFile = document.createElement("input");
		inputFile.setAttribute("type", "file");
		inputFile.setAttribute("id", "fileInput");

		buttonSubmit.setAttribute("id", "getmessage-submit");

		inputGroupName = document.createElement("input");
		inputGroupName.placeholder = "Digite o nome do grupo aqui!";

		
		inputGroupName.setAttribute("id", "group-name");
		buttonSubmit.onclick = function() {
			//Verifica se o input está vazio. Se não estiver, executa o script python
			if(unit.type != 2) {
				notEmpty = document.querySelector("#group-name").value != "";
			
				if(notEmpty){
					unit.executeScript(document.querySelector("#group-name").value);
				}
				else {
					alert("Insira algo na caixa de texto!");
				}
			}
			else {
				unit.executeScript("");
			}
			
			
		}

		selectInput = document.createElement("select");
		selectInput.setAttribute("id", "arquivoscsv");
		//glob filtra apenas os arquivos que tem contatos(algumacoisa).csv
		//Aqui será criada as options da seleção para passar ao script de
		//mandar mensagem
		glob("contatos*.csv", function (er, files) {
			files.forEach( function(element, index) {
				optionInput = document.createElement("option");
				optionInput.value = element;
				optionInput.innerHTML = element;

				selectInput.appendChild(optionInput);
			});
		})

		if(this.type != 2) {
			this.obj.appendChild(this.label1());
			this.obj.appendChild(inputGroupName);
		}
		

		if(this.type == 2){
			this.obj.appendChild(selectInput);
			this.obj.appendChild(inputMsg);
			this.obj.appendChild(inputFile)
		}

		this.obj.appendChild(buttonSubmit);
		backButton.render();
	},
	exit: function() {
		this.obj.innerHTML = "";
		mainContainer.removeChild(this.obj);
		backButton.remove();
	}
}
 // Capturar contatos:

homepage.build();
homepage.createButtons();
 

