from selenium import webdriver
import os
from time import sleep
import csv

from sys import argv

from selenium.webdriver.common.keys import Keys


#Essas informações vem do arquivo script.js

mensagem = argv[1] # Mensagem que o usuário digitou
grupo_file = argv[2] # O nome do arquivo que o usuário escolheu
arquivo_dir = argv[3] # O path arquivo que o usuário quer mandar


#arquivo_dir = r"/home/dalton/teste/salve.py"
#grupo_file = "contatosProjeto_Integrador.csv"
#mensagem = "Olá!"

with open(grupo_file, "r") as csvfile:
	contatos = list(csv.reader(csvfile, delimiter = ";"))
	print(contatos)

	grupo = contatos[0][0] #Esse indice sempre é o nome do grupo
	
	numeros = []

	for contato in contatos:
		numeros.append(contato[2])
	
	dir_path = os.getcwd()
	# O caminho do chromedriver
	chromedriver = os.path.join(dir_path, "chromedriver.exe")
	# Caminho onde será criada pasta profile
	profile = os.path.join(dir_path, "profile", "wpp")

	options = webdriver.ChromeOptions()
	# Configurando a pasta profile, para mantermos os dados da seção
	options.add_argument(r"user-data-dir={}".format(profile))
	# Inicializa o webdriver
	driver = webdriver.Chrome(
		chromedriver, chrome_options=options)
	# Abre o whatsappweb
	driver.get("https://web.whatsapp.com/")
	# Aguarda alguns segundos para validação manual do QrCode
	driver.implicitly_wait(25)

	busca = driver.find_element_by_xpath('/html/body/div/div[1]/div[1]/div[3]/div/div[1]/div/label/div/div[2]')
	busca.click()
	busca.send_keys(grupo)
	sleep(5)
	# Seleciona o grupo
	

	# numeros vem do csv


	for numero in numeros:

		grupo_elemento = driver.find_element_by_xpath("/html/body/div/div[1]/div[1]/div[3]/div/div[2]/div[1]/div/div/div[6]/div")
		grupo_elemento.click()
		sleep(0.8)
		cabecalho_elemento = driver.find_element_by_xpath("/html/body/div[1]/div/div/div[4]/div/header")
		cabecalho_elemento.click()
		# O usuário não pode ter admin no grupo.
		sleep(1)
		lupinha = driver.find_element_by_class_name('_32gq5')
		lupinha.click()

		pesquisa_usuarios = driver.find_element_by_class_name('_2_1wd')
		pesquisa_usuarios.click()
		pesquisa_usuarios.send_keys(numero)
		sleep(2)
		cs_user = driver.find_element_by_class_name('TbtXF')
		cs_user.click()
		sleep(1)

		frescura = driver.find_element_by_class_name('_11srW')
		frescura.click()

		sleep(1.5)

		if arquivo_dir != "semarquivo":
			anexarArquivo = driver.find_element_by_css_selector("span[data-icon='clip']")
			anexarArquivo.click()
			sleep(2)
			anexarArquivo2 = driver.find_element_by_xpath("/html/body/div/div[1]/div[1]/div[4]/div[1]/footer/div[1]/div[1]/div[2]/div/span/div[1]/div/ul/li[3]")
			anexarArquivo2.click()

			input_box = driver.find_element_by_tag_name("input")
			input_box.send_keys(arquivo_dir)

			sleep(3)


			enviarArquivo = driver.find_element_by_xpath("/html/body/div/div[1]/div[1]/div[2]/div[2]/span/div[1]/span/div[1]/div/div[2]/span/div/div")
			enviarArquivo.click()

		caixa_de_texto = driver.find_element_by_xpath("/html/body/div/div[1]/div[1]/div[4]/div[1]/footer/div[1]/div[2]/div/div[2]")
		caixa_de_texto.click()
		caixa_de_texto.send_keys(mensagem)
		sleep(1)
		botao_de_enviar = driver.find_element_by_xpath('/html/body/div/div[1]/div[1]/div[4]/div[1]/footer/div[1]/div[3]/button')
		botao_de_enviar.click()
		sleep(2)

		busca = driver.find_element_by_xpath('/html/body/div/div[1]/div[1]/div[3]/div/div[1]/div/label/div/div[2]')
		busca.click()
		busca.send_keys(grupo)
		sleep(5)


driver.quit()

