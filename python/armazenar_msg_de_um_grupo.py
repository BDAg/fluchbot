from selenium import webdriver
import os
from time import sleep
from bs4 import BeautifulSoup
import csv
from sys import argv

grupo = argv[1]


#print("argv = {}".format(grupo))
#print("argv type = {}".format(type(grupo)))

dir_path = os.getcwd()
# O caminho do chromedriver
chromedriver = os.path.join(dir_path, "chromedriver.exe")
# Caminho onde será criada pasta profile
profile = os.path.join(dir_path, "profile", "wpp")

options = webdriver.ChromeOptions()
# Configurando a pasta profile, para mantermos os dados da seção
options.add_argument(r"user-data-dir={}".format(profile))
# Inicializa o webdriver
driver = webdriver.Chrome(
    chromedriver, chrome_options=options)
# Abre o whatsappweb
driver.get("https://web.whatsapp.com/")
# Aguarda alguns segundos para validação manual do QrCode
driver.implicitly_wait(25)

sleep(1.5)

aba_de_pesquisa = driver.find_element_by_class_name('_2_1wd')
aba_de_pesquisa.click()
aba_de_pesquisa.send_keys(grupo)

sleep(5)

grupo_escolhido = driver.find_element_by_class_name('TbtXF')
grupo_escolhido.click()

sleep(1.5)


x = -30
vezes_0 = 0
while True:
    script = "var a = document.querySelector('._1gL0z').scrollTop;document.querySelector('._1gL0z').scrollTo(0, {}); return a".format(x)
    tamanho_tela = driver.execute_script(script)
    #print(tamanho_tela)

    if tamanho_tela == 0:
        vezes_0 = vezes_0 + 1
        #print("Passou pelo if tamanho_tela == 0: VEZES_0: {}".format(vezes_0))

    if tamanho_tela != 0 and vezes_0 < 3:
        vezes_0 = 0
        #print("Passou pelo tamanho_tela != 0 and vezes_0 < 3: VEZES_0: {}".format(vezes_0))

    if vezes_0 == 8:
        #print("TERMINOU")
        break
    sleep(1)

#script = "var a = document.querySelector('._1gL0z').scrollHeight; return a"
#tamanho_da_tela = driver.execute_script(script)

#vezes = int(tamanho_da_tela/1200) + 1
#y = 30

'''for vez in range(vezes):
    script = "document.querySelector('._1gL0z').scrollTo(0, {});".format(y)
    tamanho_tela = driver.execute_script(script)
    print(tamanho_tela)'''


container_msg = driver.find_element_by_class_name("_11liR")

soup = BeautifulSoup(container_msg.get_attribute("outerHTML"),"html.parser")

divs_msg = soup.find_all("div",{"class":"_1bR5a"})

#print(len(list(divs_msg)))

list_divs_msg = list(divs_msg)

#print(type(list_divs_msg[0]))
identificador = ""
with open('msgs{}.csv'.format(grupo.replace(" ", "")), 'w', newline='') as csvfile:
    for i in list_divs_msg:

        div_msg = str(i)
        print("I -> {}".format(i))
        soup2 = BeautifulSoup(div_msg,"html.parser")
        msg = soup2.find("div",{"class":"_3ExzF"}).contents[0]

        soup3 = BeautifulSoup(str(msg), "html.parser")

        msg2 = soup3.find('span')
        #print("MSG2: {}".format(msg2))

        soup4 = BeautifulSoup(str(msg2), "html.parser")
        msg3 = soup4.find('span')
        #print("MSG3: {}".format(msg3.contents[0]))

        msgfinal = msg3.contents[0]
        msgfinal = str(msgfinal)
        msgfinal = msgfinal.replace("<span>", "")
        msgfinal = msgfinal.replace("</span>", "")
        #msgfinal = msgfinal.replace("<span>", " ")
        #msgfinal = msgfinal.replace("</span>", " ")
        if "<img" in msgfinal:
            msgfinal = "EMOJI"
        elif "<a" in msgfinal:
            soup5 = BeautifulSoup(msgfinal, "html.parser")
            msgfinal = soup5.find('a').attrs
            msgfinal = msgfinal["href"]
            msgfinal = "Mensagem com Link {}".format(msgfinal)
        elif "<span" in msgfinal:
            msgfinal = "Alguém foi marcado nesta mensagem"
        elif '<strong class="_3-8er selectable-text copyable-text" data-app-text-template="*${appText}*">' in msgfinal:
            msgfinal = msgfinal.replace('<strong class="_3-8er selectable-text copyable-text" data-app-text-template="*${appText}*">', "")
            msgfinal = msgfinal.replace('</strong>', '')
        print("Mensagem: {}".format(msgfinal))
        #ZJv7X - Classe para número
        #_1Lc2C - Classe para nome

        #soup3 = soup2.find("div",{"class":"_3ExzF"})
        #print("Soup3 -> {}".format(soup3))
        try:
            identificador = soup2.find("div", {"class": "copyable-text"}).attrs;
        except:
            pass
        '''try:
            try:
                identificador = soup2.find("span",{"class":"ZJv7X"}).contents[0]
            except:
                identificador = soup2.find("span",{"class":"_1Lc2C"}).contents[0]
        except:
            pass'''

        #print("Pessoa -> {}".format(identificador['data-pre-plain-text']))
        horario = ""
        data = ""

        try:
            identificador = identificador['data-pre-plain-text']
            identificador = identificador.split(',')
            horario = identificador[0].replace('[', ' ')
            aux = identificador[1].split(']')
            data = aux[0].replace(']', ' ')
            nome = aux[1].replace(': ', " ")

            print("Nome -> {}".format(nome))
            print("Data -> {}".format(data))
            print("Horario -> {}".format(horario))


            msgtocsv = csv.writer(csvfile, delimiter=';')
            msgtocsv.writerow([grupo, nome, data, horario, msgfinal])
        except:
            print("Erro ao capturar mensagem")
        print("Id {}".format(identificador))

    #csv -> id, msg, hora, data

    #print("PESSOA: {}".format(identificador))
    #print("Mensagem: {}".format(msg))
driver.quit()


