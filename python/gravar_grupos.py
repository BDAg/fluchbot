#coding: utf-8
from selenium import webdriver
import os
from time import sleep
# import json
from bs4 import BeautifulSoup
import csv

from sys import argv

controle_de_tempo = 5
#grupo = argv[1]

grupo = "Big Data 2021"

dir_path = os.getcwd()
# O caminho do chromedriver
chromedriver = os.path.join(dir_path, "chromedriver.exe")
# Caminho onde será criada pasta profile
profile = os.path.join(dir_path, "profile", "wpp")

options = webdriver.ChromeOptions()
# Configurando a pasta profile, para mantermos os contatos_numero da seção
options.add_argument(r"user-data-dir={}".format(profile))
# Inicializa o webdriver
driver = webdriver.Chrome(
    chromedriver, chrome_options=options)
# Abre o whatsappweb
driver.get("https://web.whatsapp.com/")
# Aguarda alguns segundos para validação manual do QrCode
driver.implicitly_wait(25)

busca = driver.find_element_by_xpath("/html/body/div[1]/div/div/div[3]/div/div[1]/div/label/div/div[2]")
busca.click()
sleep(3)
busca.send_keys(grupo)
sleep(10)
# Seleciona o grupo
grupo_elemento = driver.find_element_by_xpath(
    "/html/body/div/div[1]/div[1]/div[3]/div/div[2]/div[1]/div/div/div[2]/div/div")
grupo_elemento.click()
#caixa_de_texto=driver.find_element_by_xpath("/html/body/div[1]/div/div/div[4]/div/footer/div[1]/div[2]/div/div[2]")
cabecalho_elemento = driver.find_element_by_xpath("/html/body/div[1]/div/div/div[4]/div/header")
cabecalho_elemento.click()
sleep(3)



try:
    mais_elementos = driver.find_element_by_xpath(
        "/html/body/div/div[1]/div[1]/div[2]/div[3]/span/div[1]/span/div[1]/div/section/div[5]/div[5]/div[1]/div")
    mais_elementos.click()
except:
    mais_elementos = driver.find_element_by_xpath(
        "/html/body/div/div[1]/div[1]/div[2]/div[3]/span/div[1]/span/div[1]/div/section/div[5]/div[3]/div[1]/div")
    mais_elementos.click()

sleep(3)

page = driver.find_element_by_class_name("_1C2Q3")

tamanho = float(page.get_attribute("scrollHeight"))
print(tamanho)

contatos = []

tamanho_da_tela_255 = 19203

qtd_max_user = 255

qtd_de_usr = (tamanho*qtd_max_user)/tamanho_da_tela_255

qtd_rodadas_255 = 29

qtd_de_rodadas = int((qtd_de_usr*qtd_rodadas_255)/qtd_max_user)+2

print('Qtd rodadas', qtd_de_rodadas)


print('qtd de usuarios', qtd_de_usr)


tamanho_por_qtd_da_tela = tamanho / qtd_de_rodadas
tamanho_ref = 0
contatos_numero = []
contatos_nome = []
for x in range(0, qtd_de_rodadas):
    script = "document.querySelector('._36Jt6').scrollTo(0, {})".format(tamanho_ref)
    driver.execute_script(script)
    tamanho_ref += tamanho_por_qtd_da_tela

    # Execultar o código de captura de contatos_numero
    grupo_membros1 = driver.find_element_by_xpath(
        "/html/body/div/div[1]/div[1]/div[2]/div[3]/span")
    html_grupo_membros1 = grupo_membros1.get_attribute('outerHTML')

    sleep(0.7)

    soup1 = BeautifulSoup(html_grupo_membros1, 'html.parser')

    contatos = soup1.find_all('span', {'class': '_35k-1'})

    contatos_numero.append(contatos)

    nomes = soup1.find_all('span', {"class": "ekBrd"})

    contatos_nome.append(nomes)

numeros = set()
nomes = set()
contatos_salvos = set()

for dado in contatos_numero:
    for data in dado:
        if str(data.contents[0])[0] == '+':
            numeros.add(str(data.contents[0]))
        else:
            contatos_salvos.add(data.contents[0])


for dado in contatos_nome:
    for data in dado:
        nomes.add(str(data.contents[0]))


'''print("Numeros: ", len(list(numeros)))
print("Nomes: ", len(list(nomes)))
print("Contatos Salvos: ", len(list(contatos_salvos)))
print(list(numeros))
print(list(nomes))
print("CONTATOS SALVOS: {}".format(contatos_salvos))'''



 #Proximo passo:
 #Criar dois vetores, um de nome e outro de números. Os dois tem que dar 38 e os indices tem que bater

if 'You' in contatos_salvos:
    contatos_salvos.remove('You')
elif 'Você' in contatos_salvos:
    contatos_salvos.remove('Você')

script = "document.querySelector('._36Jt6').scrollTo(0, 0)"
driver.execute_script(script)
'''
#Possível solução para pessoa com admin
try:
    usuario_you = driver.find_element_by_xpath("//*[@id='app']/div[1]/div[1]/div[2]/div[3]/span/div[1]/span/div[1]/div/section/div[5]/div[4]/div/div[17]/div/div/div[2]/div[1]")
except:
    usuario_you = driver.find_element_by_xpath("//*[@id='app']/div[1]/div[1]/div[2]/div[3]/span/div[1]/span/div[1]/div/section/div[5]/div[2]/div/div[10]/div/div/div[2]/div[1]")

print("Usuario You: ",usuario_you.get_attribute('outerHTML'))
soup2 = BeautifulSoup(usuario_you.get_attribute('outerHTML'), 'html.parser')
usuario_you_admin = soup2.find_all('div', {"class": "_33IQJ"})
print("Usuario You Admin: ", usuario_you_admin)

is_admin = len(usuario_you_admin)>0

print("Is Admin {}".format(is_admin))

'''
contatos_salvos_final = []
# Pega as informações dos contatos já salvos.
for i in contatos_salvos:
    grupo_elemento.click()
    sleep(0.8)
    cabecalho_elemento = driver.find_element_by_xpath("/html/body/div[1]/div/div/div[4]/div/header")
    cabecalho_elemento.click()
    #O usuário não pode ter admin no grupo.
    sleep(1)
    lupinha = driver.find_element_by_class_name('_32gq5')
    lupinha.click()
    sleep(controle_de_tempo)
    pesquisa_usuarios = driver.find_element_by_class_name('_2_1wd')
    pesquisa_usuarios.click()
    pesquisa_usuarios.send_keys(i)
    sleep(2)
    cs_user = driver.find_element_by_class_name('TbtXF')
    cs_user.click()
    sleep(1)


    frescura = driver.find_element_by_class_name('_11srW')
    frescura.click()

    sleep(1.5)


    header = driver.find_element_by_class_name('_1-qgF')
    header.click()
    html_numero = driver.find_element_by_xpath("//*[@id='app']/div[1]/div[1]/div[2]/div[3]/span/div[1]/span/div[1]/div/section/div[4]/div[3]/div/div/span").get_attribute('outerHTML')
    soup3 = BeautifulSoup(html_numero, "html.parser")
    tag_do_numero = soup3.find_all("span")
    print(tag_do_numero)
    numero_final = tag_do_numero[0].contents[0]
    contatos_salvos_final.append({"nome": i, "numero": numero_final.contents[0]})
    with open('contatos{}.csv'.format(grupo.replace(" ", "_")), 'a') as csvfile:
        csvfile.write(f'{grupo};{i};{numero_final.contents[0]} \n')
    sleep(1)


#Pegar o nome dos números armazenados
for numero in numeros:
    grupo_elemento.click()
    sleep(0.8)
    cabecalho_elemento = driver.find_element_by_xpath("/html/body/div[1]/div/div/div[4]/div/header")
    cabecalho_elemento.click()
    # O usuário não pode ter admin no grupo.
    sleep(1)
    lupinha = driver.find_element_by_class_name('_32gq5')
    lupinha.click()

    pesquisa_usuarios = driver.find_element_by_class_name('_2_1wd')
    pesquisa_usuarios.click()
    pesquisa_usuarios.send_keys(numero)
    sleep(2)
    cs_user = driver.find_element_by_class_name('TbtXF')
    cs_user.click()
    sleep(1)

    frescura = driver.find_element_by_class_name('_11srW')
    frescura.click()

    sleep(1.5)



    header = driver.find_element_by_class_name('_1-qgF')
    header.click()
    try:
        #NOME não recebe o valor que deveria receber;
        nome = driver.find_element_by_xpath('//*[@id="app"]/div[1]/div[1]/div[2]/div[3]/span/div[1]/span/div[1]/div/section/div[1]/div[2]/span/span').get_attribute('outerHTML')
        soup4 = BeautifulSoup(nome, "html.parser")
        nome = soup4.find("span", {'class', "_3e1tx"})
        nome = nome.contents[0]

        if nome == "" or nome == " " or nome == ";":
            nome = "SEM NOME"
        elif ";" in nome:
            nome.replace(";", "")
    except:
        nome = "SEM NOME"

    contatos_salvos_final.append({"nome": nome, "numero": numero})
    with open('contatos{}.csv'.format(grupo.replace(" ", "_")), 'a') as csvfile:
        csvfile.write(f'{grupo};{nome};{numero} \n')
    sleep(1)
#_2GAT7

#print("Nomes: ",nomes)
#print("Números: ", len(numeros))

#_1Kn3o _1AJnI _29Iga
driver.quit()

#_3e1tx - Nome do número

print("Final")